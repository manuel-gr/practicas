function buscarPorId() {
    const idInput = document.getElementById("inputId").value;
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users/" + idInput;

    http.onreadystatechange = function () {
        if (this.readyState === 4) {
            let res = document.getElementById('lista');

            // Limpiar la tabla antes de agregar nuevas filas
            res.innerHTML = "";

            if (this.status === 200) {
                try {
                    const datos = JSON.parse(this.responseText);

                    res.innerHTML += '<tr>' +
                        '<td>' + datos.id + '</td>' +
                        '<td>' + datos.name + '</td>' +
                        '<td>' + datos.username + '</td>' +
                        '<td>' + datos.email + '</td>' +
                        '<td>' + datos.address.street + ', ' + datos.address.suite + ', ' + datos.address.city + ', ' + datos.address.zipcode + '</td>' +
                        '<td>' + datos.phone + '</td>' +
                        '<td>' + datos.website + '</td>' +
                        '<td>' + datos.company.name + '</td>' +
                        '<td>' + datos.company.catchPhrase + '</td>' +
                        '<td>' + datos.company.bs + '</td>' +
                        '</tr>';
                } catch (error) {
                    console.error("Error en archivo JSON:", error);
                }
            } else {
                alert("Favor de seleccionar un id del (1 -10).");
            }
        }
    }

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnBuscar").addEventListener('click', buscarPorId);

document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});