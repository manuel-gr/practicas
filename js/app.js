function cargarDatos() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/albums";
    const res = document.getElementById('lista');

    // Realizar función de respuesta de petición
    http.onreadystatechange = function () {
        // Validar respuesta
        if (this.readyState === 4) {
            if (this.status === 200) {
                try {
                    const json = JSON.parse(this.responseText);

                    // Limpiar la tabla antes de cargar nuevos datos
                    res.innerHTML = "";

                    // Ciclo para mostrar los datos en la tabla
                    for (const datos of json) {
                        let row = document.createElement('tr');
                        row.innerHTML = '<td class="columna1">' + datos.userId + '</td>' +
                                        '<td class="columna2">' + datos.id + '</td>' +
                                        '<td class="columna3">' + datos.title + '</td>';
                        res.appendChild(row);
                    }
                } catch (error) {
                    console.error("Error al procesar la respuesta JSON:", error);
                }
            } else {
                console.error("Error en la petición.", this.status);
                // Manejo específico de errores según el código de estado
                if (this.status === 404) {
                    alert("Recursos no encontrados.");
                } else {
                    alert("Error en la petición.");
                }
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

// Codificar los botones
document.getElementById("btnCargar").addEventListener('click', cargarDatos);

document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});
