function cargarDatos() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/users";
    const res = document.getElementById('lista');

    http.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                // Limpiar la tabla antes de agregar nuevas filas
                res.innerHTML = "";

                try {
                    const json = JSON.parse(this.responseText);

                    if (Array.isArray(json)) {
                        for (const datos of json) {
                            res.innerHTML += '<tr>' +
                                '<td>' + datos.id + '</td>' +
                                '<td>' + datos.name + '</td>' +
                                '<td>' + datos.username + '</td>' +
                                '<td>' + datos.email + '</td>' +
                                '<td>' + (datos.address ? (datos.address.street + ', ' + datos.address.suite + ', ' + datos.address.city + ', ' + datos.address.zipcode) : '') + '</td>' +
                                '<td>' + datos.phone + '</td>' +
                                '<td>' + datos.website + '</td>' +
                                '<td>' + (datos.company ? datos.company.name : '') + '</td>' +
                                '<td>' + (datos.company ? datos.company.catchPhrase : '') + '</td>' +
                                '<td>' + (datos.company ? datos.company.bs : '') + '</td>' +
                                '</tr>';
                        }
                    } else {
                        console.error("La respuesta no es un array.");
                    }
                } catch (error) {
                    console.error("Error al parsear JSON:", error);
                }
            } else {
                console.error("Error en la petición. Estado:", this.status);
                // Manejo específico de errores según el código de estado
                if (this.status === 404) {
                    alert("Recursos no encontrados.");
                } else {
                    alert("Error en la petición.");
                }
            }
        }
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnCargar").addEventListener('click', cargarDatos);
document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});
