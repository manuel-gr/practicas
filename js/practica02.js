function cargarDatos(Idc) {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/albums/" + Idc;

    http.onreadystatechange = function () {
        if (this.readyState === 4) {
            if (this.status === 200) {
                let res = document.getElementById('lista');
                const datos = JSON.parse(this.responseText);
                res.innerHTML = '';

                if (datos.id) {
                    res.innerHTML += '<tr><td class="columna1">' + datos.userId + '</td>' +
                        '<td class="columna2">' + datos.id + '</td>' +
                        '<td class="columna3">' + datos.title + '</td></tr>';
                }
            } else {
                alert("Favor de seleccionar un id del (1 -100).");
            }
        }
    }

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnBuscar").addEventListener('click', function () {
    const Idc = document.getElementById("Idc").value;

    if (Idc.trim() !== "") {
        cargarDatos(Idc);
    } else {
        alert("Por favor, ingrese un ID válido.");
    }
});

document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});
